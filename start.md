## 参考
https://github.com/hhaAndroid/yolov5-comment


## 下载权重
bash weights/download_weights.sh

## 推理
python detect.py --weights ./weights/yolov5x.pt --img-size 1024 --conf 0.4 --source ./inference/images/ --view-img
